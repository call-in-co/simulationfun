# PROBLEM STATEMENT
**1958 Putnam Exam Problem 3A**
Real numbers are chosen at random from the interval [0,1]. Continue choosing
real numbers until the sum of the chosen numbers exceeds 1. What is the expected
value for the quantity of real numbers chosen?

Said another way, define the observing of $`X=x`$ as the following:
  1. define $`s`$ as the sum of standard uniform observations drawn; initially 
  it is 0
  2. Draw $`u\sim \text{𝕌}(0, 1)`$
     - If $`s=u_1 = 1`$ then $`X`$ takes the value 1
     - Else, set $`s`$ to the value of $`u_1`$ and continue
  3. Continue to draw $`u\sim \text{𝕌}(0,1)`$ and add its value to $`s`$ until
  $`s > 1`$
  4. The amount of draws performed to fulfill the criterion of step 3 is an
  observation of $`X`$

What is desired is the expected value of $`X`$, $`\textbf{E}[X]`$

# Intuition
Because the support of a standard uniform random variable is $`(0, 1)`$, its 
distribution function is
```math
F_{U}(u) = \Pr(U\leq u) = \frac{u - 0}{1-0} \text{I}_{(0, 1)} = u \ \text{I}_{(0, 1)}
```
in which $`\text{I}_{(a, b)}`$ is the indicator function on the interval 
$`(a, b)`$. This function _indicates_ that $`F_U(u)`$ takes the value zero
outside its argument, but as $`U`$ is a continuous random variable,
$`\Pr(U=u) = 0`$ for any $`u`$ _in_ its support. For this reason, the support of
$`X`$ does _not_ include 1: since the cumulative sum (`s`$ defined above) cannot
exceed 1 via one draw, the support of $`X`$ has an infimum of 2.

Therefore, the expected value of $`X`$, and the answer to the puzzle, is 
**at least 2**.

# Post-Simulation Reasoning
After having simulated $`1\times 10^8`$ observations of $`X`$ with the `R` code 
in `Sum_of_Standard_Uniform_Deviates_Exceeds_One.R`, the empirical expected
value for that simulation was suspiciously close to $`e`$; i.e. Euler's number,
2.71828... For now, that is a mystery that will resolve itself no doubt by
working through the probability statements analytically.
